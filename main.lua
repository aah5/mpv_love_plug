require 'mp'
utils = require 'mp.utils'
require 'ms2time'
require 'on_file_loaded'
require 'math'
love_api = require 'love_api'

--local autohidedelay = mp.get_property_number("cursor-autohide")
local ui_on = false
local ui = mp.create_osd_overlay("ass-events")
local time
local strength = 10

local function redraw_ui()
  time = ms2time(mp.get_property_number("time-pos"))

  ui.data = string.format("{\\an7}%s: %d", time, strength)
  ui:update()
  ui_on = true
  mp.set_property_bool('pause', false)
  mp.set_property_bool('pause', true)
 
  print(utils.to_string(love_api.get_toys()))
  local str = string.format("%s %d\n", time, strength)
  io.write(str)
end

local function turn_ui_off()
  ui.data = ""
  ui:update()
  ui_on = false
  mp.set_property_bool('pause', false)
end

local function toggle_ui()
  if ui_on then
    turn_ui_off()
  else
    redraw_ui()
  end
  mp.set_property("cursor-autohide", "no")
end

mp.add_key_binding("Ctrl+e", "toggle_ui", toggle_ui)

---

local function decrease_strength()
  strength = math.max(0, strength - 1)

  redraw_ui()
end

local function increase_strength()
  strength = math.min(20, strength + 1)

  redraw_ui()
end

mp.add_key_binding("Ctrl+UP", "increase_strength", increase_strength)
mp.add_key_binding("Ctrl+DOWN", "decrease_strength", decrease_strength)

-- quick strength change using number keys
for i=0,9 do
  mp.add_key_binding("Ctrl+" .. i, "set_strength_" .. i, function()
    if (i == 0) then
      strength = 20
    else
      strength = 2 * i
    end
    redraw_ui()
  end)
end
