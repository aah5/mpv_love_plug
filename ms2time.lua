local function divmod(a, b)
  return a / b, a % b
end

function ms2time(ms) 
  local minutes, remainder = divmod(ms, 60)
  local hours, minutes = divmod(minutes, 60)
  local seconds = math.floor(remainder)
  local milliseconds = math.floor((remainder - seconds) * 1000)
  local time = string.format("%02d:%02d:%02d.%03d", hours, minutes, seconds, milliseconds)

  return time
end
