--local http = require("socket.http")
--local ltn12 = require("ltn12")
local utils = require('mp.utils')

local love_api = {}

local function send_request(request)
  local body = {}
  print("before response")
  local res, code, headers, status = http.request({
    method = "POST",
    url = "http://192.168.0.117:20010/command",
    source = ltn12.source.string(utils.format_json(request)),
    headers = {
      ["Content-Type"] = "application/json",
    },
    sink = ltn12.sink.table(body)
  })

  local response = table.concat(body)
  response = utils.parse_json(response)

--  print(utils.to_string(response["data"]["toys"]))

  if response and response["data"] then
    return response["data"]
  else
    return {}
  end

  return response
end

love_api.get_toys = function()
  local data = send_request({command = "GetToys"})

  if data["toys"] then
    return utils.parse_json(data["toys"])
  else
    return {}
  end
end

return love_api
