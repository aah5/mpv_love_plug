local function init()
  local fname = mp.get_property("filename") .. ".vibe"
  local data_file = io.open(fname, "a")
  io.output(data_file)
end
mp.register_event("file-loaded", init)
